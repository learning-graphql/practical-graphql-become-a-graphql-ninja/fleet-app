import { useState } from 'react';
import { useMutation } from '@apollo/client';
import { addUser, query } from './query';

const AddUser = () => {
  const [ name, setName ] = useState('');
  const [ makeUser, { error } ] = useMutation(addUser, {
    variables: {
      name: name
    },
    refetchQueries: [
      { query: query },
    ],
    awaitRefetchQueries: true,
  });

  const nameChanged = (event) => {
    setName(event.target.value);
  };

  const resetFields = () => {
    setName('');
  };

  const submitForm = (event) => {
    event.preventDefault();
    makeUser(name);
    resetFields();
  };

  return (
    <form onSubmit={submitForm}>
      <label>
        <span>Name</span>
        <input type='text' value={name} onChange={nameChanged} />
      </label>
      <div>
        <button>Add User</button>
      </div>
      { error && <p>Error: { error.message }</p>}
    </form>
  );
};

export {
  AddUser,
};
