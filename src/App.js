import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { ApolloClient, ApolloProvider, createHttpLink, InMemoryCache } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import { isEmpty as _isEmpty } from 'lodash';
import { Navigation } from './Navigation';
import { LoginPage } from './LoginPage';
import { RegisterPage } from './RegisterPage';
import { ProfilePage } from './ProfilePage';
import './App.css';

const httpLink = createHttpLink({
  uri: 'http://localhost:3000/graphql',
});

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem('token');
  return {
    headers: {
      ...headers,
      'x-auth-token': token ? token : '',
    }
  }
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
});

const hasToken = () => !_isEmpty(localStorage.getItem('token'));

function App() {
  return (
    <div className="App">
      <ApolloProvider client={client}>
        <Router>
          <>
            <Navigation loginCheck={hasToken} />
            <hr />
            <Routes>
              <Route path='/login' element={<LoginPage />} />
              <Route path='/register' element={<RegisterPage />} />
              <Route path='/profile' element={<ProfilePage />} />
            </Routes>
          </>
        </Router>
      </ApolloProvider>
    </div>
  );
}

export default App;
