const Car = ({ id, make, model, colour }) => (
  <li key={id}>
    {make} {model} {colour}
  </li>
);

export {
  Car,
};
