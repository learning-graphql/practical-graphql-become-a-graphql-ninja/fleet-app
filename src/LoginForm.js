import { useEffect, useState } from 'react';
import { useMutation } from '@apollo/client';
import { loginUser } from './query';

const LoginForm = ({ navigate }) => {
  const [ username, setUsername ] = useState('');
  const [ password, setPassword ] = useState('');
  const [ validForm, setValidForm ] = useState(false);
  const [ login, { error, loading } ] = useMutation(loginUser, {
    variables: {
      username,
      password,
    }
  });

  useEffect(() => {
    setValidForm(username !== '' && password !== '');
  }, [username, password]);

  const usernameChanged = ({ target: { value }}) => {
    setUsername(value);
  };
  const passwordChanged = ({ target: { value }}) => {
    setPassword(value);
  };
  const submitForm = (event) => {
    event.preventDefault();
    login({
      username,
      password,
    }).then(({ data }) => {
      localStorage.setItem('token', data.login.token);
      navigate('/profile', { replace: true });
    }).catch(err => console.error(err));
  }

  return (
    <form onSubmit={submitForm}>
      <label>
        <span>Username</span>
        <input
          type="text"
          value={username}
          onChange={usernameChanged}
        />
      </label>
      <label>
        <span>Password</span>
        <input
          type="password"
          value={password}
          onChange={passwordChanged}
        />
      </label>
      <div>
        <button disabled={!validForm}>Login</button>
      </div>
      { loading && <p>Login user...</p>}
      { error && <p>Error: did not logged in user.</p>}
    </form>
  );
};

export {
  LoginForm,
};
