import { LoginForm } from './LoginForm';
import { useNavigate } from 'react-router-dom';

const LoginPage = () => {
  const navigate = useNavigate();

  return (
    <>
      <h2>Login</h2>
      <LoginForm navigate={navigate} />
    </>
  );
};

export {
  LoginPage,
};
