import { Link } from 'react-router-dom';

const NavigationNoAuth = () => (
  <ul>
    <li><Link to='/login'>Login</Link></li>
    <li><Link to='/register'>Register</Link></li>
  </ul>
);

const NavigationWithAuth = () => (
  <ul>
    <li><Link to='/profile'>Profile</Link></li>
  </ul>
);

const Navigation = ({ loginCheck }) => (
  <>
    { loginCheck() ?
      <NavigationWithAuth /> :
      <NavigationNoAuth />
    }
  </>
);

export {
  Navigation,
};
