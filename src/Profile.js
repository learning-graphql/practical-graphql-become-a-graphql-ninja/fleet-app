import { useQuery } from '@apollo/client';
import { queryMe } from './query';

const Profile = () => {
  const { data } = useQuery(queryMe);

  return (
    <>
      <h2>Hello again, {data && data.me && data.me.name}!</h2>
      { data && data.me &&
        <>
          <p>List of cars</p>
          <ul>
            {data.me.cars.length > 0 ?
              data.me.cars.map(({ id, make, model, colour }) => <li key={id}>{make} {model} ({colour})</li>) :
              <li>No cars</li>
            }
          </ul>
        </>
      }
    </>
  );
};

export {
  Profile,
};
