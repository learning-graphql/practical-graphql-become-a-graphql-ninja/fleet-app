import { Profile } from './Profile';

const ProfilePage = () => (
  <>
    <Profile />
  </>
);

export {
  ProfilePage,
};
