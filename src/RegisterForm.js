import { useEffect, useState } from 'react';
import { useMutation } from '@apollo/client';
import { registerUser } from './query';

const RegisterForm = () => {
  const [ name, setName ] = useState('');
  const [ username, setUsername ] = useState('');
  const [ password, setPassword ] = useState('');
  const [ validForm, setValidForm ] = useState(false);
  const [ success, setSuccess ] = useState(false);
  const [ register, { error, loading } ] = useMutation(registerUser, {
    variables: {
      name,
      username,
      password,
    }
  });
  useEffect(() => {
    setValidForm(name !== '' && username !== '' && password !== '');
  }, [name, username, password]);

  const submitForm = (event) => {
    event.preventDefault();
    register({
      variables: {
        name,
        username,
        password,
      }
    }).then(({ data }) => {
      setSuccess(data.register);
    }).catch(err => console.error(err));
    resetFields();
  }

  const nameChanged = ({ target: { value }}) => {
    setName(value);
  };
  const usernameChanged = ({ target: { value }}) => {
    setUsername(value);
  };
  const passwordChanged = ({ target: { value }}) => {
    setPassword(value);
  };
  const resetFields = () => {
    setName('');
    setUsername('');
    setPassword('');
    setSuccess(false);
  }

  return (
    <form onSubmit={submitForm}>
      <label>
        <span>Name</span>
        <input
          type="text"
          value={name}
          onChange={nameChanged}
        />
      </label>
      <label>
        <span>Username</span>
        <input
          type="text"
          value={username}
          onChange={usernameChanged}
        />
      </label>
      <label>
        <span>Password</span>
        <input
          type="password"
          value={password}
          onChange={passwordChanged}
        />
      </label>
      <div>
        <button disabled={!validForm}>Register</button>
      </div>
      { loading && <p>Adding user...</p>}
      { error && <p>Error: did not register user.</p>}
      { success && <p>Registration successful.</p>}
    </form>
  );
};

export {
  RegisterForm,
};
