import { RegisterForm } from './RegisterForm';

const RegisterPage = () => (
  <>
    <h2>Register</h2>
    <RegisterForm />
  </>
);

export {
  RegisterPage,
};
