import { useQuery } from '@apollo/client';
import { query } from './query';
import { UsersList } from './UsersList';
import { AddUser } from './AddUser';

const User = () => {
  const { loading, error, data } = useQuery(query);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error : {error.message}</p>;

  return <>
    <AddUser />
    <UsersList users={data.users} />
  </>;
}

export {
  User,
};
