import { Car } from './Car';

const UsersList = ({ users }) => (
  <>
    {users.map(({ id, name, cars }) => (
      <div key={id}>
        <h3>{name}</h3>
        {cars != null ?
          <ul>
            {cars.map(car => (
              <Car key={car.id} {...car}/>
            ))}
          </ul> :
          <div>No cars</div>
        }
      </div>))}
  </>
);

export {
  UsersList,
};
