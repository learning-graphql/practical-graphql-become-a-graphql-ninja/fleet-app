import { gql } from '@apollo/client';

const query = gql`
  query GetAllUsers {
      users {
          id,
          name,
          cars {
              id,
              make,
              model,
              colour,
          },
      },
  }
`;

const queryMe = gql`
  query getMe {
      me {
          id
          name
          username
          cars {
              id
              make
              model
              colour
          }
      }
  }
`;
const addUser = gql`
  mutation makeUser($name: String!) {
    makeUser(name: $name) {
      id,
      name,
      cars {
          make
      }
    }
  }
`;

const registerUser = gql`
  mutation register($name: String!, $username: String!, $password: String!) {
    register(name: $name, username: $username, password: $password)
  }
`;

const loginUser = gql`
  mutation login($username: String!, $password: String!) {
      login(username: $username, password: $password) {
          token
      }
  }
`;

export {
  query,
  addUser,
  registerUser,
  loginUser,
  queryMe,
};
